<?php
	/*
	 * enumlookup. Lookup contact information in e164.org ENUM tree
	 *
	 * Copyright (C) 2006, Michiel van Baak
	 * Thanks to evilbunny for his example script
	 *
	 * Michiel van Baak 31318787244 in the e164.org
	 *
	 * This program is free software, distributed under the terms of
	 * the GNU General Public License Version 2. See the LICENSE file
	 * at the top of the source tree.
	 */

	function enumlookup($e164org="") {
		/* check if we got a number */
		if (!strlen($e164org)) {
			die("no valid number given");
		}

		/* reverse the number and put dots between every digit */
		$arr = $number = "";
		for ($i = strlen($e164org) - 1; $i >= 0; $i--) {
			$num = mysql_escape_string($e164org[$i]);
			if ($number != "") {
				$number .= ".";
			}
			$number .= $num;
		}

		/* ask the dns for the records */
		$result = trim(`dig +short '$number.e164.org' naptr`);
		$lines = explode("\n", $result);
		foreach ($lines as $line) {
			if ($line == "") {
				continue;
			}

			while (strstr($line, "  ")) {
				$line = str_replace("  ", " ", $line);
			}
			/* parse the records into a usefull array */
			$bits  = explode(" ", $line, 5);
			$order = $bits["0"];
			$prio  = $bits["1"];
			$bits  = explode("!", $bits["4"]);
			$url   = $bits["2"];
			$arr[] = array("order" => $order, "prio" => $prio, "url" => $url);
		}

		unset($order);
		unset($prio);
		/* sort the array according to order and priority fields in enum */
		foreach ($arr as $key => $row) {
			$tmporder[$key] = $row["order"];
			$tmpprio[$key]  = $row["prio"];
		}

		array_multisort($tmporder, SORT_ASC, $tmpprio, SORT_ASC, $arr);
		
		/* start building our return value. */
		/* we will return a html string that can be displayed on the web */
		/* for an example go to http://michiel.vanbaak.info */
		/* the "contact" block there uses this function */
		$output = "";
		foreach ($arr as $key => $row) {
			/* the Address. Get rid of the \; and IDENTIFIER= strings */
			if (strtolower(substr($row["url"], 0, 8)) == "address:") {
				$row["url"] = substr($row["url"], 8);
				$bits = explode("\\", $row["url"]);
				foreach ($bits as $line) {
					$line = preg_replace("/^;/si", "", $line);
					if (strstr($line, "=")) {
						$parts = explode("=", $line);
						$output .= $parts[1]."<br />";
					}
				}
			/* the url. Add an <a> tag around it so it's clickable */
			} else if (strtolower(substr($row["url"], 0, 5)) == "http:") {
				$row["url"] = strtolower($row["url"]);
				$output .= "<a href=\"".$row["url"]."\">".$row["url"]."</a><br />";
			/* same with email address */
			} else if (strtolower(substr($row["url"], 0, 7)) == "mailto:") {
				$output .= "<a href=\"mailto:".substr($row["url"], 7)."\">".substr($row["url"], 7)."</a><br />";
			/* we omit the dnc and gpg settings */
			} else if (strtolower(substr($row["url"], 0, 4)) == "dnc:" || strtolower(substr($row["url"], 0, 4)) == "gpg:") {
				continue;
			/* everything else is printed as is */
			} else {
				$output .= $row["url"]."<br />";
			}
		}
		return $output;
	}
?>
